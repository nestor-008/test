// Cube.js configuration options: https://cube.dev/docs/config
module.exports = {
  scheduledRefreshTimeZones: ['UTC', 'Asia/Omsk'],
  scheduledRefreshContexts: async () => [
    { securityContext: { tenantId: '1' } },
    { securityContext: { tenantId: '2' } },
  ],
};
